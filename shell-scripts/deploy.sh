#!/bin/bash
###########################################################
#
#  Description : Déploiment à la volée de conteneur docker
#
#  Auteur : Nidal haddad
#
#  Date : 06/01/2020
#
###########################################################
usage(){
echo "
Options :
		- --create : lancer des conteneurs

		- --drop : supprimer les conteneurs créer par le deploy.sh

		- --infos : caractéristiques des conteneurs (ip, nom, user...)

		- --start : redémarrage des conteneurs

		- --ansible : déploiement arborescence ansible

"
}
#############
#Variables globales

idmax=`docker ps -a --format '{{.Names}}' | awk -F "-" -v user=$USER '$0 ~ user"-alpine"{print $3} ' |sort -r |head -1`


############


#si option --create
if [ "$1" == "--create" ];then

	min=$(($idmax +1  ))
	max=$(($idmax + $2 ))
	echo "valeur min est $min  "
	echo "valeur max est $max "
	echo "Début de la création du/des conteneurs"
  echo ""
	echo " notre option est --create"
	echo ""
	for i in $(seq $min $max ); do
			docker run -tid --name $USER-alpine-$i alpine:latest
		echo "conteneur $USER-alpine-$i crée "
	done

# si option --drop
elif [ "$1" == "--drop" ];then
	echo -e "Suppression des conteneurs"
	docker rm -f  $(docker ps -a  | grep alpine | awk '{print $1}')
	echo "Conteneurs supprimés"

# si option --start
elif [ "$1" == "--start" ];then

  echo ""
	echo " notre option est --start"
	docker start 	$(docker ps -a  | grep alpine | awk '{print $1}')
	echo ""

# si option --ansible
elif [ "$1" == "--ansible" ];then

  echo ""
	echo " notre option est --ansible"
	echo ""

# si option --infos
elif [ "$1" == "--infos" ];then

  echo ""
	echo " notre option est --infos"
	for container in $(docker ps -a | grep $USER-alpine | awk '{print $1}');do
		docker inspect -f '=> {{.Name}} : {{.NetworkSettings.IPAddress}}' $container
	done
	echo ""

# si aucune option affichage de l'aide
else
usage
fi
